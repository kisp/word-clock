var chai = require('chai'),
        WordClock = require('../src/WordClock');

chai.should();

describe('WordClock', function () {

    it('can initialize with default date', function () {
        var w = new WordClock();
        w.toString().should.be.a('string');
        w.getHours().should.be.a('number');
        w.getMinutes().should.be.a('number');
    });

    it('can initialize with passed in date', function () {
        var w = new WordClock(16, 00);
        w.getTime().should.be.a('string');
        w.getHours().should.equal(16);
        w.getMinutes().should.equal(0);
    });

    [
        {input: [16, 0], expected: "four o'clock"},
        {input: [16, 25], expected: "twenty-five past four"},
        {input: [16, 30], expected: "half past four"},
        {input: [16, 32], expected: "half past four"},
        {input: [16, 35], expected: "twenty-five to five"},
        {input: [12, 2], expected: "twelve o'clock"},
        {input: [11, 15], expected: "quarter past eleven"},
        {input: [11, 45], expected: "quarter to noon"},
        {input: [12, 44], expected: "twenty to one"}
    ].forEach(function (_case) {
        it('should resolve answer for ' + _case.expected, function () {
            var w = new WordClock(_case.input[0], _case.input[1]);
            w.getTime().should.equal(_case.expected);
        });
    });

    it('should normalize hours', function () {
        var w = new WordClock(16, 00);
        w.getHoursName(16).should.equal('four');
    });

    /*it('fails', function () {
     throw 'not';
     });*/
});


