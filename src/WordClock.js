var StringBuilder = require('./StringBuilder');

module.exports = WordClock;

function WordClock(hours,minutes) {
    this.dt = new Date();
    if(arguments.length>0){
        this.dt.setHours(hours);
        this.dt.setMinutes(minutes);
    }
    WordClock.prototype.getMinutes = Date.prototype.getMinutes.bind(this.dt);
    WordClock.prototype.getHours = Date.prototype.getHours.bind(this.dt);
}

WordClock.prototype.toString = function () {
    var hours = this.dt.getHours(),
            mins = this.dt.getMinutes();

    return this.translate(hours, mins);
};
WordClock.prototype.getTime = WordClock.prototype.toString;

WordClock.prototype.translate = function (hours, minutes) {
    var translated = new StringBuilder();
    if (minutes >= 35) {
        var minutesTo = this.getMinutesName(34 - (minutes - 30)),
                nextHour = this.getNextHour(hours);

        translated.appendWord(minutesTo)
                .appendWord('to')
                .appendWord(nextHour);

    } else if(minutes >= 5) {
        var minutesName = this.getMinutesName(minutes);
        translated.appendWord(minutesName)
                .appendWord('past')
                .appendWord(this.getHoursName(hours));
    } else {
        translated.appendWord(this.getHoursName(hours, true))
                .appendWord("o'clock");
    }
    return translated.toString();
};

WordClock.prototype.getNextHour = function (hours) {
    var nextHour = '';
    if (hours < 12 || hours === 23) {
        nextHour = this.getHoursName(hours + 1);
    } else {
        nextHour = this.getHoursName(hours - 11);
    }
    return nextHour;
};

WordClock.prototype.getHoursName = function (hours, skipSpecial) {
    var ret = this.specialHours[hours % 24];
    if(skipSpecial || !ret) {
        ret = this.getWordForNumber(normalizeHours(hours));
    }
    return ret;
};

function normalizeHours(hours) {
    if(hours>12) {
        hours = hours - 12;
    }
    return hours;
}

WordClock.prototype.getMinutesName = function (minutes) {
    var whatCounts = Math.floor(minutes / 5) * 5;
    return this.words[whatCounts];
};

WordClock.prototype.words = {
    0: 'zero',
    1: 'one',
    2: 'two',
    3: 'three',
    4: 'four',
    5: 'five',
    6: 'six',
    7: 'seven',
    8: 'eight',
    9: 'nine',
    10: 'ten',
    11: 'eleven',
    12: 'twelve',
    13: 'thirteen',
    14: 'fourteen',
    15: 'quarter',
    20: 'twenty',
    25: 'twenty-five',
    30: 'half'
};

WordClock.prototype.specialHours = {
    0: 'midnight',
    12: 'noon'
};

WordClock.prototype.getWordForNumber = function (num) {
    return this.words[num] || '';
};
