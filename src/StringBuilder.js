
module.exports = StringBuilder;

function StringBuilder(base) {
    this._str = base || '';
}

StringBuilder.prototype.appendWord = function (str) {
    if (this._str.length && this._str[this._str.length - 1] !== ' ') {
        this._str += ' ';
    }
    this._str += String(str);
    return this;
};

StringBuilder.prototype.toString = function () {
    return this._str;
};